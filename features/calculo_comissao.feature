# language: pt
Funcionalidade: Calculo de comissao simples
  Para estar motivado a vender mais
  Como um vendedor
  Quero saber qual o valor da comissao de uma venda
  
  Cenário: Calculo de comissao para venda de 10.000 reais
    Dado que estou na home page do app
    Quando eu entro com o valor de 10001.00 na venda
    Então a calculadora deve mostrar uma comissao de 600.06