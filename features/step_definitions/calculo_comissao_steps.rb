Dado("que estou na home page do app") do
    
  end
  
  Quando("eu entro com o valor de {float} na venda") do |valor|
    txt_valor = find_element(id: 'com.dojos.k21.interfacedojo:id/editText')
    txt_valor.send_keys valor
    btn_confirmar = find_element(id: 'com.dojos.k21.interfacedojo:id/button')
    btn_confirmar.click
  end
  
  Então("a calculadora deve mostrar uma comissao de {float}") do |valor|
    txt_mensagem = find_element(id: 'com.dojos.k21.interfacedojo:id/textView')
    expect(txt_mensagem.text).to include(valor.to_s)
  end
  